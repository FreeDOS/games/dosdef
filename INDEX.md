# DOS Defender

DOS Defender is an x86 real mode 2D shooter. Since few DOS machines still exist, the target platform is actually DOSBox, though it should work to some extent in other virtual machines. The game can be played with a mouse but it's intended to be played with a joystick/gamepad.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## DOSDEF.LSM

<table>
<tr><td>title</td><td>DOS Defender</td></tr>
<tr><td>version</td><td>1.1.0</td></tr>
<tr><td>entered&nbsp;date</td><td>2014-12-09</td></tr>
<tr><td>description</td><td>DOS Defender is an x86 real mode 2D shooter</td></tr>
<tr><td>summary</td><td>DOS Defender is an x86 real mode 2D shooter. Since few DOS machines still exist, the target platform is actually DOSBox, though it should work to some extent in other virtual machines. The game can be played with a mouse but it's intended to be played with a joystick/gamepad.</td></tr>
<tr><td>keywords</td><td>games</td></tr>
<tr><td>author</td><td>skeeto</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/skeeto/dosdefender-ld31</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Unlicense (Public Domain)](LICENSE)</td></tr>
</table>
